## Adding Short Description \(Description _excerpts_\)

The theme use [Metafields](/summary/theme-installation/shopify-metafield.md) to display Short Description.
![Product Short Description](/assets/product_short_description.png)

You may add Short Description by add Custom Field Content by 2 ways:

### Using Custom Field Chrome Extension

Find more detail [How to use Custom Field Extension](/summary/theme-installation/shopify-metafield.md) to add more detail for product page. 

You may add html format to Product metafield description excerpt.

```
<ul><li><span class="a-list-item">Wireless</span></li><li><span class="a-list-item">Motion Sensing</span></li><li><span class="a-list-item">Compatible with Several Consoles/Games</span></li><li><span class="a-list-item">Enough Action Buttons</span></li><li><span class="a-list-item">Ergonomic</span></li></ul>
```

![](/assets/metafieldproduct.png)

* Click **Save Custom Fields**

### Using the bulk editor.

Read more about The Bulk Editor : https://help.shopify.com/en/manual/productivity-tools/bulk-editing-products

1. Go to: [https://shopify.com/admin/bulk?resource_name=Product&edit=metafields.c_f.description_excerpt:string](https://shopify.com/admin/bulk?resource_name=Product&edit=metafields.c_f.description_excerpt:string)
[<button class = "markdown-button" name="button">Mass Edit Product Metafield Short Description</button>](https://shopify.com/admin/bulk?resource_name=Product&edit=metafields.c_f.description_excerpt:string)
2. Add product short description for each product.
![](/assets/description.png). You can add html code to the desription to show list as sample.
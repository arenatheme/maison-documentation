## Adding Image 360 Iframe to Product.

Take a look at the demo to see how it should end up functioning: [Demo](https://arena-Maison.myshopify.com/products/fixair-product-sample)

The theme use [Metafields](/summary/theme-installation/shopify-metafield.md) to display the iframe image 360 in product detail pages.

You may add Short Description by add Custom Field Content by 2 ways:

### Using Custom Field Chrome Extension
Read more detail about Metafield & Custom Field in sections [Shopify Metafield](/summary/theme-installation/shopify-metafield.md).
1. Enable Custom Field Chrome Extesion, add your product 360 iframe detail.  
(e.g. . https://api.cappasity.com/api/player/48783922-a97f-48aa-93e0-c98d72709285/embedded?autorun=0&amp;closebutton=1&amp;hidecontrols=0&amp;logo=1&amp;hidefullscreen=1)
![Custom Field](/assets/c_f_p.png)  
2. Click **Save Custom Fields** button  
3.  Click **Save** button  

### Using the bulk editor.

Read more about The Bulk Editor : https://help.shopify.com/en/manual/productivity-tools/bulk-editing-products

1. Go to: [https://shopify.com/admin/bulk?resource_name=Product&edit=metafields.c_f.image360:string](https://shopify.com/admin/bulk?resource_name=Product&edit=metafields.c_f.image360:string)
[<button class = "markdown-button" name="button">Mass Edit Product 360 Image Iframe</button>](https://shopify.com/admin/bulk?resource_name=Product&edit=metafields.c_f.image360:string)
2. Add image 360 iframe code to Image 360 field.
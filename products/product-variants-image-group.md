### Configure Theme Setting - Show Product Variants Images

This option will use the product variants image as small thumbnail options in product pages & grid products at homepage & collections.

![](/assets/product_variant.png)

Before Enable this option we suggest you add image to your product variant following instruction here: [<button class = "markdown-button" name="button">Add Images to Product Variant Official Document</button>](https://help.shopify.com/en/manual/products/product-variant-images)<br>

#### Steps

1. From your Shopify admin, go to **Online Store &gt; Themes.**
2. Find the theme that you want to edit and click **Customize**.
3. **From the top bar drop-down menu, select the type of page that you want to edit.**
4. Click the Product Section to customize. Eg. Product pages default
5. Go to COLOR AND SIZE SWATCHES.
6. Check **Adding Color Swatches** and Save.
7. Check **Use variant images for swatch color**. Make sure that you checked **Adding Color Swatches**  
![](/assets/product_image_swatch.png)
8, Click **Save**

### Grouping product images with variants - Variant Filter

It's support to spererate set of product thumbanails to display following product variants (e.g. Color)  
Take a look at the demo to see how it should end up functioning:  
[<button class = "markdown-button" name="button">Group Product Thumbnail Images Demo</button>](https://arena-Maison.myshopify.com/products/copy-of-fixair-product-sample)<br>

####Steps

1. Go to Products > All Products > Select Product to Edit.
2. At Images. Edit Image alt text to group image by format: **group-value** match with product variant value (handle).  
E.g. Product have color variants: Red. Black, Purple, Yellow, Orange, Blue.
Alt image should be: group-red, group-black, group-purple, group-yellow, group-orange, group-blue
![](/assets/color_variants.png)
![](/assets/group_color_1.png)
![](/assets/group_color_2.png)  
3. With the image you would like to show as thumbnail for all variants, you should edit product image alt with format: group-all.
4. Click Done when you finish edit image alt.

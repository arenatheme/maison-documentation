### Adding video to product thumbnail image by alt text

Take a look at the demo to see how it should end up functioning:  
[<button class = "markdown-button" name="button">Product Video Thumbnail AutoPlay Demo</button>](https://arena-Maison.myshopify.com/products/chambray-button-down)<br>

####Steps

1. Go to Products > All Products > Select Product to Edit.
2. At Images. Edit Image alt text to group image by format: **group-value** match with product variant value (handle).  
E.g. Product have color variants: Red. Black, Purple, Yellow, Orange, Blue.
Alt image should be: group-red, group-black, group-purple, group-yellow, group-orange, group-blue
![](/assets/color_variants.png)
![](/assets/group_color_1.png)
![](/assets/group_color_2.png)  
3. With the image you would like to show as thumbnail for all variants, you should edit product image alt with format: group-all.
4. Click Done when you finish edit image alt.

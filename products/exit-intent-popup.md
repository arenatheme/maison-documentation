## Product Discount Exit Intent Popup

[<button class = "markdown-button" name="button">Demo Discount Popup</button>](https://arena-Maison.myshopify.com/products/nike-presents-the-vapormaxa)


### Create Coupon Code
You can offer your customers a fixed value, percentage, or shipping discount on products, collections, or variants in your store. If you are selling online, then you can also offer buy X get Y discounts to encourage customers to buy items.

Follow settup instruction here: https://help.shopify.com/en/manual/promoting-marketing/discount-codes/create-discount-codes

### Adding Coupon Custom Field to Product

[<button class = "markdown-button" name="button">Add Custom Field to Product</button>](../summary/theme-installation/shopify-metafield.md)

Using 2 metafield following instruction: [Shopify Metafield name Bundle Product ](/shopify-metafield.md)

```
- namespace: c_f
- key: discount_code
- value: Add your Coupon code for the product

- namespace: c_f
- key: discount_percent
- value: Display percent of discount
```
![](/assets/metafieldproduct.png)


When navigate to Product page, add custom field for product page with Discount code & Discount percent detail.

The popup will show when people go to outside of the webpage (Intent Exit). If the customer click to apply, it's will auto apply the coupon code at checkout step.

![Discount Popups](/assets/discount_coupon_popup.png)


1. From your Shopify admin, go to **Online Store > Themes**. 
2. Find the theme that you want to configure and click **Customize > Theme settings**   
    ![Theme Setting](/assets/theme_setting.png)  

| Parameter | Description|
| - |:-:|
| [Typography](/settings/typography.md) | Font Customization. Change out all of your store’s typography|
| Icon | Configure Favicon, icon on your Online Store |
| Colors & Style | Configure Global Theme Style & Color Setting |
| General Setting | Configure Global function on your Online Store |
| Fomo Popups | Configure Fomo Popups function |
| Cart Page | Configure Shopping Cart page, function, currency | 
| Product Grid | Configure Global Setting Style to **Product Grid** |
| Newsletter Popup | Configure Mailchimp Popup | 
| Checkout | Configure Checkout Page by Shopify Default |


Click **Save**  to adapt your Setting

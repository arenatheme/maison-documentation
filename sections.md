### Theme Sections

Sections are customizable blocks of content that determine the layout of your online store. Each theme includes static sections and dynamic sections.

In order to know more How to Edit, Add, Rearrange, Hide, Remove Shopify Section, please follow Shopify Official Documentation  
[<button class = "markdown-button" name="button">Shopify Section Official Document</button>](https://help.shopify.com/en/manual/using-themes/change-the-layout/theme-settings/sections-and-settings)


#### Static sections

**Eg: Header & Footer Section **

Static sections are always in specific locations on your online store. You can [edit static sections](https://help.shopify.com/en/manual/using-themes/change-the-layout/theme-settings/sections-and-settings#edit-static-sections), but you can't rearrange or remove them. Each type of page on your online store includes different static sections.

#### Dynamic sections

You can [add](https://help.shopify.com/en/manual/using-themes/change-the-layout/theme-settings/sections-and-settings#add-dynamic-sections), [rearrange](https://help.shopify.com/en/manual/using-themes/change-the-layout/theme-settings/sections-and-settings#rearrange-dynamic-sections), and [remove](https://help.shopify.com/en/manual/using-themes/change-the-layout/theme-settings/sections-and-settings#remove-dynamic-sections) dynamic sections to customize the layout of your home page. Each theme has a unique set of dynamic sections to choose from.

---
### Home Sections
Maison includes a unique selection of home page sections
You can add, rearrange, and remove these sections to customize the layout of your home page.
### Page Sections.
Page sections belong in specific places on your online store. For example, the product pages section determines the layout of each product page on your online store.

1. From your Shopify admin, go to **Online Store > Themes**
2. Next to **Maison**, click **Customize**.
3. From the top-bar drop-down menu, select Page you would like configure
4. Use the settings to customize your Pages section  

  
Next, let's explore how to configure our theme with built-in **Sections**
### Fixed sections
Maison includes sections that show on every page of your online store. By general, there are 2 fixed sections:
    * Header
    * Footer

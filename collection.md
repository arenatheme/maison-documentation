### Create Collection

[<button class = "markdown-button" name="button">Collection - Shopify Official Document</button>](https://help.shopify.com/en/manual/products/collections)

[<button class = "markdown-button" name="button">Create an Manual Collection</button>](https://help.shopify.com/en/manual/products/collections/manual-shopify-collection#create-a-manual-collection)

[<button class = "markdown-button" name="button">Create an Automated Collection</button>](https://help.shopify.com/en/manual/products/collections/automated-collections/auto-create)

### Collection Alternate Templates
[<button class = "markdown-button" name="button">ALTERNATE TEMPLATE SHOPIFY OFFICIAL DOCUMENT</button>](https://help.shopify.com/en/themes/customization/store/create-alternate-templates)

Maison support 3 Collection Alternate templates  
![Collection template](/assets/collection_template.png)  

Parameter | Template name | Description| Demo
- |:-:|:-:|:-:
Default Collection | collection | Default collection section | [Demo](https://arena-Maison.myshopify.com/collections/all/?fts=0&preview_theme_id=35024109638)
Infinite Collection | collection.infinite | Default collection with Infinity loading product function | [Demo](https://arena-Maison.myshopify.com/collections/glasses)
Quick order form Collection | collection.order-form | Collection support quick order form to buy many products at same time | [Demo](https://arena-Maison.myshopify.com/collections/watches)

To enable the alternate Collection page style for one of your collections, navigate in your Shopify admin area to the Collection section and start editing the Collection that you want to apply the alternate product page style to.

Once you’re editing your product, find the Templates settings on the right side of the product editor. Change the template from collection to collection.alternate.  
![Collection template](/assets/collection_template.png)  

The alternate collection page has the same page settings as regular collection pages. However, it may also includes a few other options, function depend on the layout.

### Configure Collection Page by Sections

1. From your Shopify admin, go to **Online Store &gt; Themes**
2. Find the theme click **Customize**
3. From the top bar drop-down menu, select **Collection pages**/ Now you will access to edit Sections for a Collection page.
4. In the **preview of your store on the right side of the page**, navigate to your collection **uses the product.alternate** page template.
5. Open the Collection (alternate) pages settings.
6. Click Save




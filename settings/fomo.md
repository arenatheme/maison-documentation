Fomo Popups display real-time customer activity notifications on your store. You can use it as a Social Proof Markteting Tool to Boost Conversions.


### Sale Notification Popups
Popups to show the number of Sale on your Online Store to encourage customer making purchase.
![Sale Notification Popups](/assets/sale_popup.png)

**Sale Notification Popup Template**:
```
Some one in {Random City} {Random Text}.  
{Random Product in Collection}.  
{Random Number} {Random Text}  

```

Parameter | Description
 - |:-
Popup Position | Popup position to display on Desktop when lauch.<br> Position: Bottom Left/Bottom Right/Top Right/Top Left.
Show On Home Page | Show Sales Notification on Home Page
Show On Product Page | Show Sales Notification on Product Page
Show On Collection Page | Show Sales Notification On Collection Page
Hide on Mobile Device | Hide Sales Notification On Mobile Device
Random City base on | **GEOIP**: Auto Dectect Visitor Country and get list all cities to use <br> **Manual Cities List**: Input optional Cities in next field <br> **Combine both list**: Mixup cities list in visitor country & manual cities list.
Manual Cities List | Your manual cities list. Separate city by **;**
Random Text | Random text. eg: just bought;view;search
Random Product in Collection | Select product in Collection to show
Random Number | Select random number to show. Separate number by **;**
Random Text | Random text. eg: minutes ago;hours ago;days ago
Background color | Sales popup notifiction background color 
### Visitor Count Popups

Popup to show Random Visitor detail to Product page.
![Visitor Count Popups](/assets/number_visitor.png)
**Visitor Count Template**:

```
{Random Number} of visitor {Text Field}
```
Parameter | Description
- |:-
Show Visitor Count Popup | Show Visitor Count Popup when Customer visit Product page
Remove cookie every | Time gap to **Reset** the number of visitors show in the product. <br>
Counter Min | Minimum visitor number to show
Counter Max | Maxium visitor number to show
Value range of counter after reload page (Percent * The previous value) | Standard deviation range when visitor refresh product page in Web Browser.
Popup position | Popup position display in Desktop
Icon User | Icon image User icon
Text Field | text show at {Text Field}
Background color | Visitor Count Popup background color

### Discount Popup

![Discount Popups](/assets/discount_coupon_popup.png)

Configure to Product Popup Coupon. Intent Exit Popup when customer **hover out of Product page**.

[<button class = "markdown-button" name="button">Demo Discount Popup</button>](https://arena-Maison.myshopify.com/products/nike-presents-the-vapormaxa)

[<button class = "markdown-button" name="button">Configure Product Page to show Discount Popup</button>](../products/exit-intent-popup.md)

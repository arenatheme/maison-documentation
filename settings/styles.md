### Theme Style
| Parameter | Description|
| - |:-:|
| One page | each Homepage Section will display as an Homepage Slide. eg [Homepage 15](https://arena-Maison.myshopify.com/?fts=0&preview_theme_id=43291541574)|
| None | Dont use Onepage Setting|

### Website Page Layout Mode

Website Layout Mode use at all Page.

| Parameter | Description|
| - |:-:|
| Wide | Full Screen layout Mode (except page with Section Setting)|
| Boxed | Body Content in Container Box. Apply for All page|

* Boxed: Also known as fixed layouts, boxed designs consist of setting clearly defined boundaries for the main body of the page. This layout has a fixed width in **Container** that compartmentalizes all of the content.
* Wide: layout differs from a boxed one because you can use the entire screen to showcase the elements of your page. The content of the website is centred but has a white background throughout the page. Furthermore, the main body is not clearly defined. The container element has percentage widths and can, therefore, adjust according to resolution. In this case, the user does not see what the designer sees unless they both have the same screen resolution.
![Box Wide Layout](/assets/box_wide.jpg)

Some Section at the theme may use **Wide with Padding Layout** that the Wide Layout with a small spacing Padding Left & Right from the Screen.


* When you set Boxed Layout, it's will affect to all pages and Wide mode setting at **Section** will not affect.
* When you set Wide Layout, it's will affect to all pages and Boxed mode setting at **Section** still affect. It's mean that you can set layout for particular **Theme Section**.  
eg. Header Section: Wide, Product Content: Boxed, Footer: Wide. 
You can find more information with each **Theme Section** in the next section.

#### BACKGROUND BODY PAGE
At Boxed Layout, you may configure Background for outside the Boxed Container.  

| Parameter | Description|
| - |:-:|
| Color | Use Color as Background |
| Image | Use Image as Backgroudd |

#### BREADCRUMB
There are few  for Breadcrumb in Sub-pages.

### COLORS

| Parameter | Description|
| - |:-:|
| Main Color | Main color setting use for many element on site. The theme use Dark / Light Mode to auto detect text color in side background area.|
| Links Color | Colors for Links in General |
| Shop Color | Colors for Shop Function in General |
| Header Color | Colors in Header Section |
| Navigation Color | Colors in Navigation |
| Navigation Label Color | Label Colors in Navigation|
| Hamburger | Hamburger Icon Color |
| Footer Color | Colors in Footer Section |
| Product Grid Label | Colors in Product Grid |
| Button Colors | Colors for Button style 1 & 2 |
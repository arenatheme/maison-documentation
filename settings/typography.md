#### FONTS

You can add a selection of premium fonts to your theme by using Shopify's font library.  
Shopify's font library is a collection of fonts that includes system fonts, a selection of Google fonts, and licensed fonts from [Monotype](https://www.monotype.com/). These fonts are free to use on Shopify online stores, and are provided in both [WOFF](https://caniuse.com/#feat=woff) and [WOFF2](https://caniuse.com/#feat=woff2) formats.

![](/assets/Shopifyfont.png)

[<button class = "markdown-button" name="button">Shopify Font Library Official Documentation</button>](https://help.shopify.com/en/themes/development/fonts/library)

  
  

| Parameter | Description|
| - |:-:|
|Header font| Font use for heading |
|Body Font| Font use for body

#### FONT SIZE (PIXELS)

| Parameter | Description|
| - |:-:|
|Heading 1 | H1 font Size |
|Heading 2 | H2 font Size |
|Heading 3 | H3 font Size |
|Heading 4 | H4 font Size |
|Heading 5 | H5 font Size |
|Heading 6 | H6 font Size |
|Top Bar font size | Font Size for text at Top bar Section |
|Product grid title size | Product name font size at Product Grid |
|Product grid price size | Product price font size at Product Grid |
|Product item title size | Product name font size at Product Page |
|Product item price size | Product price font size at Product Page |
|Navigation size | Navigation text font size |
|Navigation dropdown size| Navigation text at dropdown (sub-level) font size|
| Body size| Body text font size|

#### Favicon
Favicons are tiny icons that appear next to your site’s title or URL in browser tabs, bookmark menus and address bars. The recommended image dimensions are 16px by 16px.

#### Loading Icon
Default Loading GIFs image (.gif) icon 

#### General Icon

You may upload an Image or use Icon font class by ArenaFont. In order to use Arena Font Icon please following instruction about [Arena Font](how-to-use-and-update-arenafont)  

| Parameter | Description|
| - |:-:|
| Account Icon | Icon for Account Login |
| Search Icon | Icon for Search button at Search Forms|
| Wishlist Icon | Icon for Wishlist Function|
| Compare Icon | Icon for Compare Function|
| Cart Icon | Icon for Cart Function redirects to Cart Page|
| Social Sharing Icon | Icon for Social Sharing Fuction |


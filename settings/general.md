### General

| Parameter | Description|
| - |:-:|
| Enable Lazy Loading| Enable Lazy loadding with image loading to optimize your online store speed.|
| Button Back To Top| Enable button Back to Top on Desktop|
| Button Search Function| Enable Auto Suggestion Search|
| Enable Google Translate| Enable Free Google Translate for your Online Store |
| Enable Wishlist & Compare App| Show Wishlist Compare icon on your Online Store | 
| Effect Falling| Enable Leaf Falling Effect|

### Quickview
| Parameter | Description|
| - |:-:|
| Quick View Product| Enable Quickview Option|
| Popup| Popup Quickview when click Quickview icon|
| Drawer| Drawer Quickview when click Quickview icon|

### Product Default Template

Select default Product Template to Product. More detail about [Page alternate template](/pages.md#page-alternate-templates)

### Collection Page

Configure settings at Collection Pages (use for all collection alternate templates)  

Parameter | Description
- |:-:
Mode view default| Select collection default template for Product Grid / List
Number of Product per row| How many products show per row at Product Grid / List
Enable product's image blance | Auto crop image with similar Image Height
Enable product's title blance | Auto balance product title long

### EU Cookie Policy

| Parameter | Description|
| - |:-:|
| EU Cookie Policy| Enable EU Cookie Policy  |
| EU Cookie AutoDetect| Auto Detect Visitor from EU Country to show EU Cookie Policy by GeoIP Service|
| Position | Position to show EU Cookie Popup on Desktop <br>Bottom/Top/Floating left/Floating right|
| Link to own policy | Customer will go out and vistit the link if they dont alow to use Cookie |
| EU Cookie Color Options | Configure color for EU Cookie Popup |